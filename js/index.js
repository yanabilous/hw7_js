let inputArr = ["hello", "world", 23, "23", null, undefined];
let typeToCheck = "object";

function filterBy(arr,type) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        if ((typeof arr[i]) !== type) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

function filterByV2(arr,type) {
    return arr.filter(arrItem => typeof arrItem !== type)
}

console.log(filterBy(inputArr, typeToCheck));
console.log(filterByV2(inputArr, typeToCheck));

